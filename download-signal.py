#!/usr/bin/env python3

import os
import sys
import json
import urllib.request

download_path = os.path.join(os.environ['HOME'], '.cache/fdroidcl/apks/')

if len(sys.argv) > 1:
    download_path = sys.argv[1]

if not os.path.exists(download_path):
    os.makedirs(download_path)

with urllib.request.urlopen("https://updates.signal.org/android/latest.json") as latest_url:
    latest_fresh = json.loads(latest_url.read().decode())

filename = latest_fresh['url'].split('/')[-1]

if filename in os.listdir(download_path):
    print("{} already exists in {}\n".format(filename, download_path))
else:
    downloaded_filename, http_headers = urllib.request.urlretrieve(latest_fresh['url'], filename=os.path.join(download_path, filename))
    print("Signal successfully downloaded to {}!".format(os.path.join(download_path, filename)))

os.symlink(os.path.join(download_path, filename), os.path.join(download_path, 'org.thoughtcrime.securesms.apk'))
sys.exit(0)

