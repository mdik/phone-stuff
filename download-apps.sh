#!/bin/bash

/usr/bin/fdroidcl update

if adb shell pm list packages | grep -q 'foundation.e.apps'; then
	APPS=$(grep -v -e nextcloud -e owncloud -e davdroid -e opencamera -e aurora $1)
else
	APPS=$(cat $1)
fi

if adb shell pm list packages | grep -q 'com.generalmagic.magicearth'; then
	adb shell pm disable-user com.generalmagic.magicearth
fi

for app in $APPS; do
	if [ "$app" = "org.thoughtcrime.securesms" ]; then
		python3 ./download-signal.py
		adb install ~/.cache/fdroidcl/apks/org.thoughtcrime.securesms.apk
	else
		/usr/bin/fdroidcl install $app;
	fi
done

if adb shell pm list packages | grep -q 'net.osmand.plus'; then
    wget -c -O ~/.cache/fdroidcl/World_basemap_2.obf.zip 'https://download.osmand.net/download?standard=yes&file=World_basemap_2.obf.zip'
    unzip -n ~/.cache/fdroidcl/World_basemap_2.obf.zip -d ~/.cache/fdroidcl/
    adb push ~/.cache/fdroidcl/World_basemap_2.obf /sdcard/Android/data/net.osmand.plus/files/World_basemap.obf
    
    wget -c -O ~/.cache/fdroidcl/Germany_berlin_europe_2.obf.zip 'https://download.osmand.net/download?standard=yes&file=Germany_berlin_europe_2.obf.zip'
    unzip -n ~/.cache/fdroidcl/Germany_berlin_europe_2.obf.zip -d ~/.cache/fdroidcl/
    adb push ~/.cache/fdroidcl/Germany_berlin_europe_2.obf /sdcard/Android/data/net.osmand.plus/files/Germany_berlin_europe.obf
fi
